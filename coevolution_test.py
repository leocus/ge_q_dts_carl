import os
import gym
import pybulletgym  # register PyBullet enviroments with open ai gym
import string
import datetime
import stopit
import argparse
import numpy as np
import pandas as pd
from numpy import random
from continuous_optimization import UMDAc, CMAES, DE
from dt import EpsGreedyLeaf, PythonDT, RandomlyInitializedEpsGreedyLeaf
from grammatical_evolution import GrammaticalEvolutionTranslator, GrammaticalEvolution, UniformMutator, OnePointCrossover, ReplaceIfBetter, TournamentSelection, BestSelection, NoReplacement, ReplaceWithOldIfWorse


def string_to_dict(x):
    """
    This function splits a string into a dict.
    The string must be in the format: key0-value0#key1-value1#...#keyn-valuen
    """
    result = {}
    items = x.split("#")

    for i in items:
        key, value = i.split("-")
        try:
            result[key] = int(value)
        except:
            try:
                result[key] = float(value)
            except:
                result[key] = value

    return result


parser = argparse.ArgumentParser()
parser.add_argument("--jobs", default=1, type=int, help="The number of jobs to use for the evolution")
parser.add_argument("--seed", default=0, type=int, help="Random seed")
parser.add_argument("--environment_name", type=str, default="LunarLanderContinuous-v2", help="The name of the environment in the OpenAI Gym framework")
parser.add_argument("--n_actions", default=4, type=int, help="The number of action that the agent can perform in the environment")
parser.add_argument("--output_size", default=2, type=int, help="The size of the action for the environment")
parser.add_argument("--learning_rate", default="auto", help="The learning rate to be used for Q-learning. Default is: 'auto' (1/k)")
parser.add_argument("--df", default=0.9, type=float, help="The discount factor used for Q-learning")
parser.add_argument("--eps", default=1, type=float, help="Epsilon parameter for the epsilon greedy Q-learning")
parser.add_argument("--input_space", default=8, type=int, help="Number of inputs given to the agent")
parser.add_argument("--episodes", default=50, type=int, help="Number of episodes that the agent faces in the fitness evaluation phase")
parser.add_argument("--episode_len", default=1000, type=int, help="The max length of an episode in timesteps")
parser.add_argument("--ge_pop_size", default=30, type=int, help="Population size for the GE")
parser.add_argument("--mutation_decay_enabled", action="store_true", help="Enables decay for the mutation")
parser.add_argument("--mutation_rate_decay", type=float, default=0.95, help="Decay for the mutation")
parser.add_argument("--mutation_rate_minimum", type=float, default=float("-inf"), help="Decay for the mutation")
parser.add_argument("--eda_pop_size", default=30, type=int, help="Population size for the EDA")
parser.add_argument("--eda_selection_size", default=10, type=int, help="Number of individuals selected in the EDA")
parser.add_argument("--update_eda_each", type=int, default=1, help="Number of generations between each update of the EDA algorithm.")
parser.add_argument("--n_evaluations_per_generation", default=150, type=int, help="Number of individuals selected in the EDA")
parser.add_argument("--generations", default=1000, type=int, help="Number of generations")
parser.add_argument("--cxp", default=0.1, type=float, help="Crossover probability")
parser.add_argument("--mp", default=1, type=float, help="Mutation probability")
parser.add_argument("--gene_prob", default=0.1, type=float, help="Gene mutation probability")
parser.add_argument("--tournament_size", default=2, type=int, help="Size of the tournament")
parser.add_argument("--genotype_len", default=100, type=int, help="Length of the fixed-length genotype")
parser.add_argument("--decay", default=0.99, type=float, help="The decay factor for the epsilon decay (eps_t = eps_0 * decay^t)")
parser.add_argument("--patience", default=50, type=int, help="Number of episodes to use as evaluation period for the early stopping")
parser.add_argument("--timeout", default=600, type=int, help="Maximum evaluation time, useful to continue the evolution in case of MemoryErrors")
parser.add_argument("--with_bias", action="store_true", help="if used, then the the condition will be (sum ...) < <const>, otherwise (sum ...) < 0")
parser.add_argument("--random_init", action="store_true", help="Randomly initializes the leaves in [-1, 1[")
parser.add_argument("--constant_range", default=1, type=float, help="Max magnitude for the constants being used (multiplied *10^-3). Default: 1000 => constants in [-1, 1]")
parser.add_argument("--constant_step", default=0.001, type=float, help="Step used to generate the range of constants, mutliplied *10^-3")
parser.add_argument("--types", default=None, type=str, help="This string must contain the range of constants for each variable in the format '#min_0,max_0,step_0,divisor_0;...;min_n,max_n,step_n,divisor_n'. All the numbers must be integers. The min and the max of this range (for each variable) are used to normalize the variables.")
parser.add_argument("--no_replacement", action="store_true", help="if used, no replacement technique will be used.")
parser.add_argument("--old_replacement", action="store_true", help="if used, the old replacement technique will be preferred.")
parser.add_argument("--use_cmaes", action="store_true", help="if used, uses CMA-ES to optimize the actions.")
parser.add_argument("--use_de", action="store_true", help="if used, uses DE to optimize the actions.")
parser.add_argument("--best_selection", action="store_true", help="if used, uses BestSelection instead of tournament.")
parser.add_argument("--print_exc", action="store_true", help="if used, prints the exceptions raised during fitness evaluation.")
parser.add_argument("--orthogonal", action="store_true", help="if used, orthogonal trees are evolved.")
parser.add_argument("--pairwise", action="store_true", help="if used, orthogonal trees are evolved.")
parser.add_argument("--render", action="store_true", help="if used, renders the environment.")
parser.add_argument("--linear_actions", action="store_true", help="if used, actions are not constant but depend on the inputs.")
parser.add_argument("--low", type=float, default=-1, help="Lower bound for random initialization. Default: -1.")
parser.add_argument("--up", type=float, default=1, help="Upper bound for random initialization. Default: 1.")
parser.add_argument("--action_scale", type=float, default=1, help="Scaling factor for the actions performed. Default: 1.")
parser.add_argument("--interval_min", type=float, default=-1, help="Init interval for UMDA. Default: -1.")
parser.add_argument("--interval_max", type=float, default=1, help="Init interval for UMDA. Default: 1.")
parser.add_argument("--extended_logging", action="store_true", help="if used, logs a lot of stuff.")


# Setup logging

date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
logdir = "logs/gym/{}_{}".format(date, "".join(np.random.choice(list(string.ascii_lowercase), size=8)))
logfile = os.path.join(logdir, "log.txt")
os.makedirs(logdir)

args = parser.parse_args()

best = None
lr = "auto" if "." not in args.learning_rate else float(args.learning_rate)


# Creation of the EpsilonDecay Leaf
class EpsilonDecayLeaf(RandomlyInitializedEpsGreedyLeaf):
    """A eps-greedy leaf with epsilon decay."""

    def __init__(self):
        """
        Initializes the leaf
        """
        if not args.random_init:
            RandomlyInitializedEpsGreedyLeaf.__init__(
                self,
                n_actions=args.n_actions,
                learning_rate=lr,
                discount_factor=args.df,
                epsilon=args.eps,
                low=0,
                up=0
            )
        else:
            RandomlyInitializedEpsGreedyLeaf.__init__(
                self,
                n_actions=args.n_actions,
                learning_rate=lr,
                discount_factor=args.df,
                epsilon=args.eps,
                low=args.low,
                up=args.up
            )

        self._decay = args.decay
        self._steps = 0

    def get_action(self):
        self.epsilon = self.epsilon * self._decay
        self._steps += 1
        return super().get_action()


# Setup Grammar

input_space_size = args.input_space

types = args.types if args.types is not None else ";".join(["-10,11,1,10" for _ in range(input_space_size)])
types = types.replace("#", "")
# assert len(types.split(";")) == input_space_size, "Expected {} types, got {}.".format(input_space_size, len(types.split(";")))
if len(types.split(";")) < input_space_size:
    types += ";" + ";".join([types.split(";")[-1]] * (input_space_size - len(types.split(";"))))

consts = {}
for index, type_ in enumerate(types.split(";")):
    rng = type_.split(",")
    start, stop, step, divisor = map(int, rng)
    consts_ = list(map(str, [float(c) / divisor for c in range(start, stop, step)]))
    consts[index] = consts_

oblique_split = "+".join(["<const> * (_in_{0} - {1})/({2} - {1})".format(i, consts[i][0], consts[i][-1]) for i in range(input_space_size)])

grammar = {
    "bt": ["<if>"],
    "if": ["if <condition>:{<action>}else:{<action>}"],
    "action": ["out=_leaf;leaf=\"_leaf\"", "<if>"],
    # "const": ["0", "<nz_const>"],
    "const": ["{:.3f}".format(k) for k in np.arange(-args.constant_range, args.constant_range+args.constant_step, args.constant_step)],
    # "var": ["_in_{}".format(i) for i in range(input_space_size)],
    "varsplit": ["_in_{0} < <const_{0}>".format(i) for i in range(input_space_size)],
    "twovarsplit": ["<const> * <normvar> < <const> * <normvar> + <const>".format(i) for i in range(input_space_size)],
    "normvar": ["(_in_{0} - {1})/({2} - {1})".format(i, consts[i][0], consts[i][-1]) for i in range(input_space_size)],
}

if args.orthogonal:
    grammar["condition"] = ["<varsplit>"]
else:
    if not args.with_bias:
        grammar["condition"] = [oblique_split + " < 0" for i in range(1, input_space_size + 1)]
        # grammar["condition"] = ["+ <const> * <var>" * i + " < 0" for i in range(1, input_space_size + 1)]
    else:
        grammar["condition"] = [oblique_split + " < <const>" for i in range(1, input_space_size + 1)]
        # grammar["condition"] = ["+ <const> * <var>" * i + " < <const>" for i in range(1, input_space_size + 1)]
if args.pairwise:
    grammar["condition"].append("<twovarsplit>")
for i in range(input_space_size):
    grammar[f"const_{i}"] = consts[i]

# print(grammar)

# Seeding of the random number generators

random.seed(args.seed)
np.random.seed(args.seed)


# Log variables

with open(logfile, "a") as f:
    vars_ = locals().copy()
    for k, v in vars_.items():
        if "grammar" not in k:
            f.write("{}: {}\n".format(k, v))


# Definition of the fitness function

def evaluate_fitness(fitness_function, leaf, individual, actions, episodes=args.episodes):
    phenotype, _ = GrammaticalEvolutionTranslator(grammar).genotype_to_str(individual.get_genes())
    bt = PythonDT(phenotype, leaf)
    return fitness_function(bt, actions, episodes, timeout=args.timeout)


@stopit.threading_timeoutable(default=((-10000,), None))
def fitness(tree, actions, episodes=args.episodes):
    random.seed(args.seed)
    np.random.seed(args.seed)
    global_cumulative_rewards = []
    if "CarRacing" in args.environment_name:
        e = gym.make(args.environment_name, verbose=0)
    else:
        e = gym.make(args.environment_name)
    initial_perf = None
    if not args.linear_actions:
        actions_available = actions.reshape(args.n_actions, args.output_size)
    else:
        actions_available = actions.reshape(args.n_actions, args.output_size, 2 * (input_space_size + 1))
    try:
        for iteration in range(episodes):
            e.seed(iteration)
            obs = e.reset()
            tree.new_episode()
            cum_rew = 0
            action = 0

            for t in range(args.episode_len):
                obs = list(obs.flatten())

                decision = tree(obs)
                if not args.linear_actions:
                    action = args.action_scale * actions_available[decision]
                else:
                    action_selected = actions_available[decision]
                    action = []

                    for model in action_selected:
                        mask = model[:input_space_size + 1] > 0
                        coefficients = model[input_space_size + 1:]
                        masked_model = np.zeros(input_space_size + 1)
                        masked_model[mask] = coefficients[mask]
                        action.append(np.dot(masked_model, [*obs[:len(masked_model) - 1], 1]))

                obs, rew, done, info = e.step(action)
                if args.render:
                    e.render()

                tree.set_reward(rew)

                cum_rew += rew

                if done:
                    break

            obs = list(obs.flatten())
            tree(obs)
            global_cumulative_rewards.append(cum_rew)

            # Check stopping criterion

            if initial_perf is None and iteration >= args.patience:
                initial_perf = np.mean(global_cumulative_rewards)
            elif iteration % args.patience == 0 and iteration > args.patience:
                if np.mean(global_cumulative_rewards[-args.patience:]) - initial_perf < 0:
                    break
                initial_perf = np.mean(global_cumulative_rewards[-args.patience:])
    except Exception as ex:
        if args.print_exc:
            print(ex)  # If it comes here saying that action[0] is a list it is because the tree was not valid, so it returned None
        if len(global_cumulative_rewards) == 0:
            global_cumulative_rewards = [-10000]
    e.close()

    fitness = np.mean(global_cumulative_rewards[-args.patience:])
    return fitness, tree.leaves, global_cumulative_rewards


if __name__ == '__main__':
    from joblib import parallel_backend, Parallel, delayed

    def fit_fcn(x, a):
        return evaluate_fitness(fitness, EpsilonDecayLeaf, x, a)

    def map_function(f, couples):
        return Parallel(args.jobs)(delayed(f)(i, a) for i, a in couples)

    longest_production = max(map(len, grammar.values()))
    max_value = 1000 * longest_production
    mutator = UniformMutator(args.gene_prob, max_value)
    if args.no_replacement:
        replacement = NoReplacement()
    else:
        if args.old_replacement:
            replacement = ReplaceWithOldIfWorse(os.path.join(logdir, "rif.log") if args.extended_logging else None)
        else:
            replacement = ReplaceIfBetter(os.path.join(logdir, "rif.log") if args.extended_logging else None)

    if args.best_selection:
        selection = BestSelection(os.path.join(logdir, "sel.log") if args.extended_logging else None)
    else:
        selection = TournamentSelection(args.tournament_size, os.path.join(logdir, "sel.log") if args.extended_logging else None)

    best = -float("inf")
    ge = GrammaticalEvolution(pop_size=args.ge_pop_size, mutation=mutator, crossover=OnePointCrossover(), selection=selection, replacement=replacement, mut_prob=args.mp, cx_prob=args.cxp, genotype_length=args.genotype_len, max_int=max_value, logdir=logdir if args.extended_logging else None)
    if not args.linear_actions:
        if args.use_de:
            eda = DE(args.eda_pop_size, [(args.interval_min, args.interval_max)] * (args.n_actions * args.output_size), 0.9, 0.5)
        elif args.use_cmaes:
            eda = CMAES(args.n_actions * args.output_size, args.eda_pop_size)
        else:
            eda = UMDAc(args.eda_selection_size, args.eda_pop_size, args.n_actions * args.output_size, (args.interval_min, args.interval_max), logdir if args.extended_logging else None)
    else:
        if args.use_de:
            eda = DE(args.eda_pop_size, [(args.interval_min, args.interval_max)] * (args.n_actions * args.output_size * 2 *(input_space_size + 1)), 0.9, 0.5)
        elif args.use_cmaes:
            eda = CMAES(args.n_actions * args.output_size * 2 *(input_space_size + 1), args.eda_pop_size)
        else:
            eda = UMDAc(args.eda_selection_size, args.eda_pop_size, args.n_actions * args.output_size * 2 * (input_space_size + 1), (args.interval_min, args.interval_max), logdir if args.extended_logging else None)


    history = {"Max": [], "Min": [], "Mean": [], "Std": []}

    print("Max\tMean\tStd\tMin")
    with parallel_backend("multiprocessing"):
        for gen in range(args.generations):
            individuals = ge.ask()
            if gen % args.update_eda_each == 0:
                actions = eda.ask()
                action_temp_fitnesses = [[] for _ in range(len(actions))]

            if args.mutation_decay_enabled:
                mutator._gene_probability = min(args.mutation_rate_minimum, mutator._gene_probability * args.mutation_rate_decay)

            if args.extended_logging:
                with open(logfile, "a") as log_:
                    log_.write("[{}][GENERATION] Generation {} started.\n".format(datetime.datetime.now(), gen))
                    for j, individual in enumerate(individuals):
                        log_.write("[INDIVIDUAL] Individual {}\n".format(j))
                        log_.write("{}\n".format(individual))
                    for j, action in enumerate(actions):
                        log_.write("[ACTIONS] Set {}\n".format(j))
                        log_.write("{}\n".format(action))

                    log_.write("[{}][GENERATION] Coupling started.\n".format(datetime.datetime.now()))

            individuals_to_evaluate = np.random.randint(0, len(individuals), size=args.n_evaluations_per_generation)
            actions_to_evaluate = np.random.randint(0, len(actions), size=args.n_evaluations_per_generation)

            for i in range(len(individuals)):
                if i not in individuals_to_evaluate:
                    individuals_to_evaluate = np.append(individuals_to_evaluate, [i])

            for i in range(len(actions)):
                if i not in actions_to_evaluate:
                    actions_to_evaluate = np.append(actions_to_evaluate, [i])

            if len(actions_to_evaluate) < len(individuals_to_evaluate):
                actions_to_evaluate = np.append(actions_to_evaluate, np.random.randint(0, len(actions), len(individuals_to_evaluate) - len(actions_to_evaluate)))
            elif len(actions_to_evaluate) > len(individuals_to_evaluate):
                individuals_to_evaluate = np.append(individuals_to_evaluate, np.random.randint(0, len(individuals), len(actions_to_evaluate) - len(individuals_to_evaluate)))

            couples = [(individuals[i], actions[j]) for i, j in zip(individuals_to_evaluate, actions_to_evaluate)]

            if args.extended_logging:
                with open(logfile, "a") as log_:
                    log_.write("[{}][GENERATION] Coupling finished. Couples:\n".format(datetime.datetime.now()))
                    for i, couple in enumerate(couples):
                        log_.write("[COUPLE] Couple {}\n".format(i))
                        log_.write("Tree: {}; Set: {}\n".format(individuals_to_evaluate[i], actions_to_evaluate[i]))

            fitnesses = map_function(fit_fcn, couples)
            if args.extended_logging:
                with open(logfile, "a") as log_:
                    for j, fitnessvalue in enumerate(fitnesses):
                        log_.write("[FITNESS] Couple {} has fitness: {}\n".format(j, fitnessvalue[0]))
                        log_.write("Leaves of tree \n".format(j))
                        log_.write("{}\n".format(fitnessvalue[1]))
                        log_.write("Fitness history of the tree {}\n".format(j))
                        log_.write("{}\n".format(fitnessvalue[-1]))
            else:
                for j, fitnessvalue in enumerate(fitnesses):
                    if fitnessvalue[0] > best:
                        best = fitnessvalue[0]
                        with open(logfile, "w") as log_:
                            for k, v in [("Namespace", args), ("Grammar", grammar)]:
                                log_.write("{}: {}\n".format(k, v))

                            log_.write("[{}][GENERATION] Generation {} started.\n".format("Undefined", gen))
                            log_.write("[INDIVIDUAL] Individual {}\n".format(individuals_to_evaluate[j]))
                            log_.write("{}\n".format(individuals[individuals_to_evaluate[j]]))
                            log_.write("[ACTIONS] Set {}\n".format(actions_to_evaluate[j]))
                            log_.write("{}\n".format(actions[actions_to_evaluate[j]]))
                            log_.write("[FITNESS] Couple {} has fitness: {}\n".format(j, fitnessvalue[0]))
                            log_.write("Leaves of tree \n".format(j))
                            log_.write("{}\n".format(fitnessvalue[1]))
                            log_.write("Fitness history of the tree {}\n".format(j))
                            log_.write("{}\n".format(fitnessvalue[-1]))


            fitness_values = np.array([f[0] for f in fitnesses])
            individuals_fitnesses = [np.mean(fitness_values[individuals_to_evaluate == i]) for i in range(len(individuals))]
            for i in range(len(actions)):
                action_temp_fitnesses[i].extend(fitness_values[actions_to_evaluate == i])

            """
            print(individuals_fitnesses)
            print(actions_fitnesses)
            """
            ge.tell(individuals_fitnesses)
            if gen % args.update_eda_each == 0:
                actions_fitnesses = [np.mean(action_temp_fitnesses[i]) for i in range(len(actions))]
                eda.tell(actions_fitnesses)

            fitnessvalues = [f[0] for f in fitnesses]
            print(("{:.2f}\t"*4).format(np.max(fitnessvalues), np.mean(fitnessvalues), np.std(fitnessvalues), np.min(fitnessvalues)))
            history["Max"].append(np.max(fitnessvalues))
            history["Min"].append(np.min(fitnessvalues))
            history["Mean"].append(np.mean(fitnessvalues))
            history["Std"].append(np.std(fitnessvalues))

            with open(logfile, "a") as log_:
                log_.write("[STATS] " + ("{:.2f}\t"*4).format(np.max(fitnessvalues), np.mean(fitnessvalues), np.std(fitnessvalues), np.min(fitnessvalues)) + "\n")
                log_.write("[GLOBALBEST] " + "{:.2f}\t".format(np.max(history["Max"])) + "\n")

        pd.DataFrame.from_dict(history).to_csv(os.path.join(logdir, "history.csv"))
