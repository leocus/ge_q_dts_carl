#!/usr/bin/python3
"""
Utilities for the optimization of behaviourTree

Author: Leonardo Lucio Custode
Creation Date: 08-04-2020
Last modified: mar 5 mag 2020, 23:33:35
"""
import gym
import time
import DecisionTree


def evaluate_dt_on_task(dt: DecisionTree, task: gym.Env, seed: int = 0, max_steps: int = 10000, render: bool = False):
    """
    This function evaluates the passed behavior tree on the passed task
    """
    total_score = 0

    # Seed the environment
    task.seed(seed)
    # Start a new episode
    observation = task.reset()
    reward = 0

    for iteration in range(max_steps):
        # Update the internal reward of the tree
        dt.set_reward(reward)

        # Get the next action (and update the Q-value of the previous)
        action = dt.get_action(observation.flatten())

        # Perform action in the environment
        observation, reward, done, _ = task.step(action)
        if render:
            task.render()
            time.sleep(0.05)

        total_score += reward

        if done:
            # If the task is finished, break the loop
            break

    # Update the internal reward of the tree with the last reward for the episode
    dt.set_reward(reward)

    # Update the Q-value of the previous
    dt.get_action(observation.flatten())

    if render:
        print("Total score: {}".format(total_score))
    task.close()
    return total_score

