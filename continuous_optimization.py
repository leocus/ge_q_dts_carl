#!/bin/python3
"""
This file contains the implementation of estimation of distribution algorithms
"""
import os
import cmaes
import numpy as np


class ContinuousOptimizer():
    """
    A base class for continuous optimizers
    """

    def ask(self):
        """
        Returns the current population
        """
        raise NotImplementedError("This method must be implemented by the extending class")

    def tell(self, fitnesses):
        """
        Assigns the fitnesses to the members of the population

        :fitnesses: A list (with the same order as the individuals returned by tell) of floats
        """
        raise NotImplementedError("This method must be implemented by the extending class")


class EDA(ContinuousOptimizer):
    """Ask-tell abstract-class for estimation of distribution algorithms"""

    def __init__(self, selection_size, lambda_, n_params):
        """Initializes an EDA

        :selection_size: The number of individuals used for selection
        :lambda_: The number of the samples to generate
        :n_params: The number of params to optimize

        """
        self._selection_size = selection_size
        self._lambda_ = lambda_
        self._n_params = n_params

    def ask(self):
        return self._generate()

    def tell(self, fitnesses):
        indices = sorted([*range(len(fitnesses))], key=lambda x: fitnesses[x], reverse=True)
        self._log_info("Sorted fitnesses: " + str(np.array(fitnesses)[indices]))
        return self._update(indices)

    def _generate(self):
        raise NotImplementedError("This method must be implemented by the extending class")

    def _update(self, indices):
        raise NotImplementedError("This method must be implemented by the extending class")

    def _log_info(self, fname):
        raise NotImplementedError("This method must be implemented by the extending class")


class UMDAc(EDA):
    """Implementation of the UMDAc algorithm"""

    def __init__(self, selection_size, lambda_, n_params, bounds=(-1, 1), logdir=None):
        """
        Initializes an instance of the UMDAc algorithm.

        :selection_size: the number of individuals that are selected to sample the next gen
        :lambda_: the number of total individuals
        :n_params: The number of params to optimize
        :bounds: bounds to initialize mean and stdev
        :logdir: Path of the dir where the log must be saved

        """
        EDA.__init__(self, selection_size, lambda_, n_params)

        mean = (bounds[1] - bounds[0]) / 2

        self._mean = np.zeros(self._n_params) + mean
        self._std = np.ones(self._n_params) * max(abs(bounds[1] - mean), abs(bounds[0] - mean))
        self._pop = []
        self._logfile = os.path.join(logdir, "eda.log") if logdir is not None else None
        self._log_info("μ: {}, σ: {}".format(self._mean, self._std))

    def _generate(self):
        self._pop = np.random.normal(self._mean, self._std, (self._lambda_, self._n_params))
        self._log_info("Generated the following individuals:\n" + "\n".join(str(x).replace("\n", "") for x in self._pop))
        return self._pop

    def _update(self, indices):
        self._log_info("The best are {}".format(indices[:self._selection_size]))
        selected = self._pop[indices][:self._selection_size]
        self._mean = np.mean(selected, axis=0).flatten()
        self._std = np.std(selected, axis=0).flatten()
        for i in range(len(self._std)):
            if np.isnan(self._std[i]):
                self._std[i] = 0.01
        self._log_info("μ: {}, σ: {}".format(str(self._mean).replace("\n", ""), str(self._std).replace("\n", "")))

    def _log_info(self, msg):
        if self._logfile is not None:
            with open(self._logfile, "a") as f:
                f.write("[UMDAc] {}\n".format(msg))


class CMAES(EDA):
    """Wrapper for CMA-ES"""

    def __init__(self, n_params, lambda_=None):
        """
        Initializes an instance of CMA-ES

        :n_params: The number of params to optimize
        :lambda_: Population size
        """
        EDA.__init__(self, None, lambda_, n_params)

        self._cmaes = cmaes.CMA([0] * n_params, 1, population_size=lambda_)

    def _generate(self):
        self._individuals = [self._cmaes.ask() for _ in range(self._cmaes.population_size)]
        return self._individuals

    def tell(self, fitnesses):
        self._cmaes.tell([(a, -f) for a, f in zip(self._individuals, fitnesses)])


class DE(ContinuousOptimizer):

    """
    Implementation of the differential evolution.
    """

    def __init__(self, pop_size, bounds, CR, F):
        """
        Initializes an isntance of the DE.

        :pop_size:
        :bounds:
        :CR:
        :F:

        """
        ContinuousOptimizer.__init__(self)

        self._pop_size = pop_size
        self._CR = CR
        self._F = F
        self._bounds = bounds

        population = np.random.uniform([b[0] for b in bounds], [b[1] for b in bounds], (pop_size, len(bounds)))
        self.pop = [{"Parent": None, "Fitness": None, "Individual": p} for p in population]

    def ask(self):
        """
        Returns the current population
        """
        return [p["Individual"] for p in self.pop]

    def tell(self, fitnesses):
        """
        Assigns the fitness to the individuals and computes the next generation

        :fitnesses: A list of floats/integers
        """
        assert len(fitnesses) == len(self.pop)
        for i in range(len(fitnesses)):
            f = fitnesses[i]

            if self.pop[i]["Parent"] is None or f > self.pop[i]["Parent"]["Fitness"]:
                self.pop[i]["Fitness"] = f
            else:
                self.pop[i] = self.pop[i]["Parent"]
                self.pop[i]["Parent"] = None

        for p in self.pop:
            assert p["Fitness"] is not None
        new_pop = []

        for i in range(len(self.pop)):
            i1, i2, i3, i4 = np.random.choice(self.pop, size=4, replace=False)
            x1, x2, x3, x4 = map(lambda x: x["Individual"], [i1, i2, i3, i4])
            v = x1 + self._F * (x2 - x3)
            rnd = np.random.uniform(size=len(v))
            j = np.random.randint(0, len(v))
            u = x4
            u[rnd < self._CR] = v[rnd < self._CR]
            u[j] = v[j]
            new_pop.append({"Parent": i4, "Fitness": None, "Individual": u})

        self.pop = new_pop[:]
        
