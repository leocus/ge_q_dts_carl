#!/usr/bin/python3
"""
Tests

Author: Leonardo Lucio Custode
"""
import pytest
import random
import numpy as np
from copy import deepcopy
from grammatical_evolution import *
n_runs = 10

@pytest.mark.parametrize("seed", range(n_runs))
def test_uniform_mutator0(seed):
    np.random.seed(seed)
    max_ = 100
    individual = Individual(np.random.randint(0, max_, 100))
    new = individual.copy()
    UniformMutator(1, max_)(new)

    assert len(new._genes) == len(individual._genes), "The mutated individual must have the same number of genes"
    for gene in new._genes:
        assert isinstance(gene, int) or isinstance(gene, np.int64), type(gene)
        assert gene <= max_, ("A gene cannot exceed the max value")

@pytest.mark.parametrize("seed", range(n_runs))
def test_uniform_mutator1(seed):
    np.random.seed(seed)
    max_ = 100
    individual = Individual(np.random.randint(0, max_, 100))
    new = individual.copy()
    UniformMutator(1, max_)(new)

    assert len(new._genes) == len(individual._genes), "The mutated individual must have the same number of genes"
    for gene in new._genes:
        assert isinstance(gene, int) or isinstance(gene, np.int64)
        assert gene <= max_, ("A gene cannot exceed the max value")


@pytest.mark.parametrize("seed", range(n_runs))
def test_uniform_mutator2(seed):
    np.random.seed(seed)
    max_ = 100
    individual = Individual(np.random.randint(0, max_, 100))
    new = individual.copy()
    UniformMutator(0, max_)(new)

    assert len(new._genes) == len(individual._genes), "The mutated individual must have the same number of genes"
    assert np.linalg.norm(new._genes - individual._genes) == 0


@pytest.mark.parametrize("seed", range(n_runs))
def test_opc0(seed):
    np.random.seed(seed)
    max_ = 100
    individuals = [Individual(np.random.randint(0, max_, 100)) for _ in range(2)]

    offs = OnePointCrossover()(*individuals)

    switched = False
    for o1g, i1g, i2g in zip(offs[0]._genes, *[i._genes for i in individuals]):
        assert isinstance(o1g, int) or isinstance(o1g, np.int64)
        assert isinstance(i1g, int) or isinstance(i1g, np.int64)
        assert isinstance(i2g, int) or isinstance(i2g, np.int64)
        if not switched:
            if o1g == i1g:
                continue
            else:
                assert o1g == i2g, "Unknown gene"
                switched = True
        else:
            assert o1g == i2g


@pytest.mark.parametrize("seed", range(n_runs))
def test_rib0(seed):
    np.random.seed(seed)
    max_ = 100
    individuals = [Individual(np.random.randint(0, max_, 100)) for _ in range(2)]
    offspring = [deepcopy(i) for i in individuals]
    [UniformMutator(0, max_)(i) for i in offspring]

    assert len(individuals) == len(offspring)
    for j, (i, o) in enumerate(zip(individuals, offspring)):
        i._fitness = np.random.uniform(0, 1)
        o._parents = [i]
        o._fitness = 2 * i._fitness

    result = ReplaceIfBetter()(individuals, offspring)

    for r, o in zip(result, offspring):
        assert r == o


@pytest.mark.parametrize("seed", range(n_runs))
def test_rib1(seed):
    np.random.seed(seed)
    max_ = 100
    individuals = [Individual(np.random.randint(0, max_, 100)) for _ in range(2)]
    offspring = [deepcopy(i) for i in individuals]
    [UniformMutator(0, max_)(i) for i in offspring]

    assert len(individuals) == len(offspring)
    for j, (i, o) in enumerate(zip(individuals, offspring)):
        i._fitness = np.random.uniform(0, 1)
        o._parents = [i]
        o._fitness = 1/2 * i._fitness

    result = ReplaceIfBetter()(individuals, offspring)

    for r, i in zip(result, individuals):
        assert r == i


@pytest.mark.parametrize("seed", range(n_runs))
def test_rib2(seed):
    np.random.seed(seed)
    max_ = 100
    individuals = [Individual(np.random.randint(0, max_, 100)) for _ in range(2)]
    offspring = [deepcopy(i) for i in individuals]
    [UniformMutator(0, max_)(i) for i in offspring]

    assert len(individuals) == len(offspring)
    for j, (i, o) in enumerate(zip(individuals, offspring)):
        i._fitness = np.random.uniform(0, 1)
        o._parents = [i]
        o._fitness = i._fitness

    result = ReplaceIfBetter()(individuals, offspring)

    for r, i in zip(result, individuals):
        assert r == i


@pytest.mark.parametrize("seed", range(n_runs))
def test_rib3(seed):
    np.random.seed(seed)
    max_ = 100
    individuals = [Individual(np.random.randint(0, max_, 100)) for _ in range(2)]
    offspring = [deepcopy(i) for i in individuals]
    [UniformMutator(0, max_)(i) for i in offspring]

    assert len(individuals) == len(offspring)
    for j, (i, o) in enumerate(zip(individuals, offspring)):
        i._fitness = np.random.uniform(0, 1)
        o._parents = [i]
        o._fitness = np.random.uniform(0, 1) * i._fitness

    result = ReplaceIfBetter()(individuals, offspring)

    for r, i, o in zip(result, individuals, offspring):
        assert r == i or r == o

def test_rib4():
    max_ = 100
    old_pop = [Individual(np.random.randint(0, max_, 100)) for _ in range(1000)]
    new_pop = [Individual(np.random.randint(0, max_, 100)) for _ in range(1000)]

    for index, ind in enumerate(old_pop):
        ind._fitness = index % 2
        print(ind._fitness, end=" ")
    print()

    for index, ind in enumerate(new_pop):
        ind._parents = [old_pop[index]]
        ind._fitness = 2 * ((index) % 2) - 1
        print(ind._fitness, end=" ")
    print()

    replaced_population = ReplaceIfBetter()(old_pop, new_pop)
    assert len(replaced_population) == len(old_pop)
    assert len(replaced_population) == len(new_pop)

    for index, ind in enumerate(replaced_population):
        print(ind._fitness, end=" ")
    print()
    for index, ind in enumerate(replaced_population):
        if index % 2 == 0:
            assert ind in old_pop  # 0 better than -1
        else:
            assert ind in new_pop  # 1 geq 1


def test_rib5():
    max_ = 100
    old_pop = [Individual(np.random.randint(0, max_, 100)) for _ in range(1000)]
    new_pop = [Individual(np.random.randint(0, max_, 100)) for _ in range(1000)]

    for index, ind in enumerate(old_pop):
        ind._fitness = index % 2

    for index, ind in enumerate(new_pop):
        ind._parents = [old_pop[index]]
        ind._fitness = index % 2 + 1

    replaced_population = ReplaceIfBetter()(old_pop, new_pop)

    for index, ind in enumerate(replaced_population):
        assert ind in new_pop


def test_rib6():
    max_ = 100
    old_pop = [Individual(np.random.randint(0, max_, 100)) for _ in range(1000)]
    new_pop = [Individual(np.random.randint(0, max_, 100)) for _ in range(1000)]

    for index, ind in enumerate(old_pop):
        ind._fitness = index % 2

    for index, ind in enumerate(new_pop):
        if index < len(old_pop) - 1:
            ind._parents = [old_pop[index]]
        else:
            if index % 2 == 0:
                ind._parents = [old_pop[index], old_pop[index + 1]]
            else:
                ind._parents = [old_pop[index - 1], old_pop[index]]

        ind._fitness = 0.5

    replaced_population = ReplaceIfBetter()(old_pop, new_pop)

    for index, ind in enumerate(replaced_population):
        if index % 2 == 0:
            assert ind in new_pop
        else:
            assert ind in old_pop  # Because the only individual with fitness 1
            # will replace the worst parent (Fitness 0)


@pytest.mark.parametrize("seed", range(n_runs))
def test_ts0(seed):
    np.random.seed(seed)
    max_ = 100
    n = 2

    fitnesses = [0] * 1000
    fitnesses[1] = 1

    result = TournamentSelection(10)(fitnesses)
    assert len(result) == len(fitnesses)
    assert 1 in result

# ================================================================================
# ||                            Grammatical Evolution                           ||
# ================================================================================
seeds = np.arange(0, n_runs, 1, dtype=int)
mutations = [UniformMutator(0.1, 100), UniformMutator(0.5, 100), UniformMutator(1, 1000)]
crossovers = [OnePointCrossover()]
selections = [TournamentSelection(2), TournamentSelection(3), TournamentSelection(4), BestSelection()]
replacements = [ReplaceIfBetter()]
popsize = 10
generations = 10

combinations = []

for m in mutations:
    for c in crossovers:
        for s in selections:
            for r in replacements:
                for seed in seeds:
                    combinations.append([seed, m, c, s, r])


@pytest.mark.parametrize("seed, mutation, crossover, selection, replacement", combinations)
def test_ge_ask_noreplace(seed, mutation, crossover, selection, replacement):
    import time
    np.random.seed(seed)
    random.seed(seed)

    ge = GrammaticalEvolution(popsize, mutation, crossover, selection, replacement, 0.5, 0.5, 100)

    ids = set()
    individuals = ge.ask()
    ge.tell(np.ones(popsize))

    for g in np.arange(0, generations, 1):
        new_individuals = ge.ask()

        ge.tell(np.zeros(popsize))

        assert len(ge._old_individuals) == len(ge._individuals) == popsize

@pytest.mark.parametrize("seed, mutation, crossover, selection, replacement", combinations)
def test_ge_ask_replace(seed, mutation, crossover, selection, replacement):
    import time
    np.random.seed(seed)
    random.seed(seed)

    ge = GrammaticalEvolution(popsize, mutation, crossover, selection, replacement, 0.5, 0.5, 100)

    individuals = ge.ask()[:]
    ge.tell(np.zeros(popsize))

    for i in individuals:
        assert i in ge._individuals

    for g in np.arange(1, generations + 1, 1):
        ge.ask()
        ge.tell(np.ones(popsize) * g)

        for i in ge._individuals:
            assert i._fitness == g

        for oi in ge._old_individuals:
            assert oi._fitness == g - 1

        for i in individuals:
            assert i._fitness < g

        for idx, i in enumerate(individuals):
            for gei in ge._individuals:
                assert i is not gei

        for idx, i in enumerate(ge._individuals):
            assert i._fitness == g


@pytest.mark.parametrize("seed, mutation, crossover, selection, replacement", combinations)
def test_ge_ask_replacesome(seed, mutation, crossover, selection, replacement):
    import time
    np.random.seed(seed)
    random.seed(seed)

    ge = GrammaticalEvolution(popsize, mutation, crossover, selection, replacement, 1., 1., 100)

    individuals = ge.ask()
    ge.tell(np.zeros(popsize))

    for g in np.arange(1, generations + 1, 1):
        new_individuals = ge.ask()
        fitnesses = np.ones(popsize) * g
        mask = np.random.randint(0, 2, popsize)
        ge.tell(fitnesses * mask)

        for oi in ge._old_individuals:
            assert oi._fitness <= g - 1

        oldcount = 0
        newcount = 0
        for gei in ge._individuals:
            found = False
            for n in new_individuals:
                if sum(gei._genes != n._genes) == 0:
                    newcount += 1
                    found = True
                    break
            if found:
                continue
            for i in ge._old_individuals:
                if sum(gei._genes != i._genes) == 0:
                    oldcount += 1
                    found = True
                    break
            if not found:
                print(gei)

        assert len(new_individuals) == popsize
        assert len(individuals) == popsize
        assert oldcount + newcount == popsize

        individuals = new_individuals[:]

@pytest.mark.parametrize("seed, mutation, crossover, selection, replacement", combinations)
def test_ge_nochanges(seed, mutation, crossover, selection, replacement):
    np.random.seed(seed)
    random.seed(seed)

    ge = GrammaticalEvolution(popsize, mutation, crossover, selection, replacement, 0., 0., 100)

    individuals = []

    ge.ask()
    ge.tell(np.zeros(popsize))
    for g in range(generations):
        individuals = ge.ask()
        for i in individuals:
            assert i in ge._individuals
            assert i in ge._old_individuals
        ge.tell(np.random.uniform(0, 10, popsize))

@pytest.mark.parametrize("seed, mutation, crossover, selection, replacement", combinations)
def test_ge_ask_bug0(seed, mutation, crossover, selection, replacement):
    np.random.seed(seed)
    random.seed(seed)

    ge = GrammaticalEvolution(popsize, mutation, crossover, selection, replacement, np.random.uniform(0, 1), np.random.uniform(0, 1), 100)

    individuals = []

    for g in range(generations):
        ge.ask()
        ge.tell(np.random.uniform(0, 1, popsize))
        individuals.append(deepcopy(ge._individuals))

    ctr = 0
    for idx, pop in enumerate(individuals):
        if idx == 0:
            continue
        for ind in pop:
            if ind._parents is not None:
                individual_fitnesses = [float(p._fitness) for p in ind._parents]
                assert ind._fitness >= min(individual_fitnesses), idx
            else:
                ctr += 1

